#include <stdio.h>
#include <stdlib.h>
struct nodo {
       int data;
       struct nodo *next;
};

typedef struct nodo SList;


void print_list(SList *n)
{
    printf("Lista:");
    for(; n != NULL; n = n->next)
          printf("\t%d",n->data);
    printf("\n");
}

void agregar_nodo(SList *lst, struct nodo n)
{
        for(;lst != NULL; lst = lst->next);
        lst->next = &n;
        return;
}

SList *crear(int n, int data[]){
        int i;
        SList *tmp;
        SList *res = (SList *) malloc(sizeof(struct nodo));
        tmp = res;
        //for(i = 0; i < n; i++){
        for(i = 0; i < n; i++, res = res->next){
                res->data = data[i];
                res->next = (struct nodo *) malloc(sizeof(struct nodo)) ;
        }

        return tmp;
}

void destroy(SList *lst){
        for(; lst != NULL; lst = lst->next)
                free(lst);
        SList *tmp;
        for(;lst != NULL;){
                tmp = lst->next;
                free(lst);
                lst = tmp;
        }
}

int main(){
    struct nodo n1, n2, n3,n4;
    SList *lst;

    /*
     * lst = &n1;
     * 
     * n1.data = 12;
     * n1.next = &n2;

     * n2.data = 99;
     * n2.next = &n3;

     * n3.data = 37;
     * n3.next = NULL;

     * n4.data = 666;
     * n4.next = NULL;

     * print_list(lst);
     * agregar_nodo(lst,n4);
     * print_list(lst);
     */
    // Otra historia
    int lista[] = {12, 99, 37};
    lst = crear(3,lista);
    print_list(lst);
    
    return 0;
}
